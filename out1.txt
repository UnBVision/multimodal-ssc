
MMNet Training Script

Selected device: cuda:0
Cuda available: True
Available devices  2
Current cuda device: 0 (GeForce RTX 3080)
Train:  795 Valid:  654
Input type: depth+rgb+normals
Class class weights: [1, 1, 1, 1, 1, 2, 1, 1, 2, 2, 1, 2]
Training model R294_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.7-Int-3.0
mIoU improved from 0.00000 to  0.14819
ceil        :   0.0        floor       :  92.6        wall        :  25.4        window      :   1.2        
chair       :   2.2        bed         :   0.0        sofa        :   0.0        table       :   4.4        
tvs         :   0.0        furniture   :  13.7        objects     :  23.6        
mIoU improved from 0.14819 to  0.19285
ceil        :   0.6        floor       :  91.6        wall        :  21.1        window      :  15.2        
chair       :  17.3        bed         :  10.2        sofa        :   1.6        table       :   9.4        
tvs         :   0.0        furniture   :  22.4        objects     :  22.8        
mIoU improved from 0.19285 to  0.21025
ceil        :   0.6        floor       :  92.1        wall        :  18.1        window      :  16.6        
chair       :  20.1        bed         :  18.4        sofa        :   7.2        table       :   4.0        
tvs         :   0.0        furniture   :  28.0        objects     :  26.2        
mIoU improved from 0.21025 to  0.27319
ceil        :  17.4        floor       :  92.2        wall        :  23.6        window      :  15.7        
chair       :  11.9        bed         :  46.1        sofa        :  19.6        table       :  17.6        
tvs         :   0.0        furniture   :  28.1        objects     :  28.5        
mIoU improved from 0.27319 to  0.28510
ceil        :  15.1        floor       :  91.0        wall        :  24.8        window      :  17.5        
chair       :  24.9        bed         :  47.7        sofa        :  27.1        table       :  15.4        
tvs         :   0.1        furniture   :  32.0        objects     :  18.0        
mIoU improved from 0.28510 to  0.30629
ceil        :  22.1        floor       :  92.3        wall        :  22.5        window      :  21.2        
chair       :  25.4        bed         :  44.9        sofa        :  35.6        table       :  18.7        
tvs         :   0.2        furniture   :  29.3        objects     :  24.7        
mIoU improved from 0.30629 to  0.30989
ceil        :  19.9        floor       :  91.9        wall        :  25.9        window      :  22.2        
chair       :  26.0        bed         :  46.4        sofa        :  21.5        table       :  15.7        
tvs         :   0.0        furniture   :  44.0        objects     :  27.4        
mIoU improved from 0.30989 to  0.32568
ceil        :  22.5        floor       :  92.6        wall        :  28.1        window      :  16.6        
chair       :  27.6        bed         :  51.7        sofa        :  30.5        table       :  11.9        
tvs         :   0.5        furniture   :  47.3        objects     :  28.8        
mIoU improved from 0.32568 to  0.34643
ceil        :  23.6        floor       :  92.4        wall        :  28.5        window      :  25.7        
chair       :  28.6        bed         :  52.2        sofa        :  31.0        table       :  14.9        
tvs         :   6.6        furniture   :  49.1        objects     :  28.4        
mIoU improved from 0.34643 to  0.36638
ceil        :  23.6        floor       :  90.8        wall        :  29.2        window      :  31.2        
chair       :  23.9        bed         :  60.0        sofa        :  41.4        table       :  24.6        
tvs         :  10.4        furniture   :  39.7        objects     :  28.2        
mIoU improved from 0.36638 to  0.36907
ceil        :  25.2        floor       :  92.5        wall        :  34.3        window      :  29.4        
chair       :  27.6        bed         :  56.4        sofa        :  34.6        table       :  19.6        
tvs         :  11.6        furniture   :  48.8        objects     :  26.1        
mIoU 0.30281 was not an improvement from 0.36907
ceil        :  11.0        floor       :  92.7        wall        :  28.0        window      :  25.7        
chair       :  22.2        bed         :  36.0        sofa        :  22.4        table       :  10.3        
tvs         :  13.1        furniture   :  46.4        objects     :  25.5        
mIoU improved from 0.36907 to  0.39845
ceil        :  26.6        floor       :  92.5        wall        :  34.4        window      :  31.4        
chair       :  32.6        bed         :  68.5        sofa        :  49.0        table       :  19.3        
tvs         :  16.7        furniture   :  36.3        objects     :  31.0        
mIoU 0.37006 was not an improvement from 0.39845
ceil        :  26.8        floor       :  92.4        wall        :  30.9        window      :  26.0        
chair       :  29.1        bed         :  51.0        sofa        :  44.3        table       :  19.7        
tvs         :  14.4        furniture   :  42.6        objects     :  29.8        
mIoU 0.34178 was not an improvement from 0.39845
ceil        :  21.1        floor       :  92.6        wall        :  29.4        window      :  24.4        
chair       :  26.3        bed         :  53.7        sofa        :  31.3        table       :  16.9        
tvs         :  15.5        furniture   :  37.0        objects     :  27.8        
mIoU improved from 0.39845 to  0.40405
ceil        :  30.4        floor       :  92.5        wall        :  34.0        window      :  31.6        
chair       :  28.5        bed         :  61.5        sofa        :  50.2        table       :  22.1        
tvs         :  20.9        furniture   :  43.7        objects     :  29.1        
mIoU 0.26075 was not an improvement from 0.40405
ceil        :  20.1        floor       :  91.7        wall        :  32.4        window      :  24.4        
chair       :   6.1        bed         :   9.4        sofa        :  25.2        table       :  12.0        
tvs         :  15.4        furniture   :  26.3        objects     :  23.9        
mIoU 0.37384 was not an improvement from 0.40405
ceil        :  23.4        floor       :  92.4        wall        :  32.7        window      :  28.5        
chair       :  31.5        bed         :  47.9        sofa        :  47.5        table       :  19.1        
tvs         :  20.1        furniture   :  39.4        objects     :  28.8        
mIoU 0.39560 was not an improvement from 0.40405
ceil        :  18.6        floor       :  92.8        wall        :  35.2        window      :  26.4        
chair       :  32.4        bed         :  64.6        sofa        :  51.0        table       :  17.1        
tvs         :  21.5        furniture   :  46.9        objects     :  28.7        
mIoU improved from 0.40405 to  0.41410
ceil        :  26.9        floor       :  91.4        wall        :  34.2        window      :  29.7        
chair       :  31.6        bed         :  68.8        sofa        :  54.1        table       :  27.7        
tvs         :  21.0        furniture   :  41.3        objects     :  28.9        
mIoU 0.35514 was not an improvement from 0.41410
ceil        :  33.2        floor       :  92.4        wall        :  31.3        window      :  28.8        
chair       :  21.4        bed         :  53.6        sofa        :  31.3        table       :  16.3        
tvs         :  16.6        furniture   :  38.5        objects     :  27.2        
mIoU improved from 0.41410 to  0.42158
ceil        :  29.7        floor       :  91.9        wall        :  34.0        window      :  25.7        
chair       :  36.1        bed         :  65.8        sofa        :  55.4        table       :  24.5        
tvs         :  23.4        furniture   :  48.1        objects     :  29.2        
mIoU 0.33956 was not an improvement from 0.42158
ceil        :  21.2        floor       :  92.1        wall        :  32.8        window      :  19.7        
chair       :  27.0        bed         :  48.2        sofa        :  24.1        table       :  13.0        
tvs         :  19.4        furniture   :  47.6        objects     :  28.4        
mIoU 0.37532 was not an improvement from 0.42158
ceil        :  30.2        floor       :  92.5        wall        :  30.6        window      :  21.7        
chair       :  31.7        bed         :  64.0        sofa        :  37.0        table       :  14.8        
tvs         :  13.8        furniture   :  49.8        objects     :  26.8        
mIoU improved from 0.42158 to  0.42617
ceil        :  30.0        floor       :  92.5        wall        :  36.3        window      :  29.9        
chair       :  35.7        bed         :  68.1        sofa        :  52.3        table       :  26.7        
tvs         :  23.7        furniture   :  44.1        objects     :  29.5        
mIoU 0.32781 was not an improvement from 0.42617
ceil        :  20.3        floor       :  91.6        wall        :  41.7        window      :  24.3        
chair       :  29.9        bed         :  27.8        sofa        :  24.8        table       :  10.7        
tvs         :  16.7        furniture   :  45.9        objects     :  26.9        
mIoU 0.34655 was not an improvement from 0.42617
ceil        :  24.1        floor       :  91.4        wall        :  33.0        window      :  22.3        
chair       :  29.8        bed         :  48.9        sofa        :  30.8        table       :  12.5        
tvs         :  17.3        furniture   :  47.2        objects     :  23.8        
mIoU 0.41970 was not an improvement from 0.42617
ceil        :  34.8        floor       :  92.3        wall        :  31.5        window      :  32.3        
chair       :  32.8        bed         :  66.4        sofa        :  49.0        table       :  24.2        
tvs         :  20.4        furniture   :  46.7        objects     :  31.3        
mIoU 0.35329 was not an improvement from 0.42617
ceil        :  24.4        floor       :  92.3        wall        :  34.9        window      :  25.0        
chair       :  24.9        bed         :  49.6        sofa        :  40.8        table       :  21.9        
tvs         :   9.5        furniture   :  37.5        objects     :  27.8        
mIoU 0.30381 was not an improvement from 0.42617
ceil        :  21.4        floor       :  92.0        wall        :  36.8        window      :  26.0        
chair       :  14.0        bed         :  20.6        sofa        :  33.1        table       :  16.7        
tvs         :  19.6        furniture   :  30.7        objects     :  23.2        
mIoU 0.41934 was not an improvement from 0.42617
ceil        :  42.8        floor       :  92.2        wall        :  33.7        window      :  28.6        
chair       :  29.9        bed         :  61.7        sofa        :  48.2        table       :  20.3        
tvs         :  29.4        furniture   :  43.9        objects     :  30.5        
mIoU 0.41017 was not an improvement from 0.42617
ceil        :  36.6        floor       :  92.4        wall        :  27.1        window      :  25.6        
chair       :  29.4        bed         :  66.2        sofa        :  50.2        table       :  19.7        
tvs         :  27.9        furniture   :  45.8        objects     :  30.2        
mIoU 0.39843 was not an improvement from 0.42617
ceil        :  35.9        floor       :  92.1        wall        :  41.3        window      :  21.3        
chair       :  34.1        bed         :  56.3        sofa        :  43.1        table       :  15.7        
tvs         :  21.5        furniture   :  50.0        objects     :  27.1        
mIoU 0.39548 was not an improvement from 0.42617
ceil        :  37.1        floor       :  92.2        wall        :  31.5        window      :  24.5        
chair       :  31.0        bed         :  63.3        sofa        :  42.5        table       :  15.5        
tvs         :  21.1        furniture   :  47.4        objects     :  29.0        
mIoU 0.38045 was not an improvement from 0.42617
ceil        :  27.1        floor       :  92.2        wall        :  33.6        window      :  25.3        
chair       :  33.9        bed         :  53.0        sofa        :  41.2        table       :  14.0        
tvs         :  22.6        furniture   :  49.2        objects     :  26.4        
mIoU 0.40558 was not an improvement from 0.42617
ceil        :  28.7        floor       :  92.0        wall        :  33.0        window      :  28.3        
chair       :  35.5        bed         :  67.8        sofa        :  44.6        table       :  20.6        
tvs         :  16.8        furniture   :  47.2        objects     :  31.7        
mIoU 0.40393 was not an improvement from 0.42617
ceil        :  30.6        floor       :  92.3        wall        :  37.9        window      :  26.2        
chair       :  33.5        bed         :  60.2        sofa        :  41.8        table       :  23.8        
tvs         :  24.8        furniture   :  42.5        objects     :  30.8        
mIoU 0.38711 was not an improvement from 0.42617
ceil        :  35.6        floor       :  92.5        wall        :  32.0        window      :  24.4        
chair       :  30.5        bed         :  63.9        sofa        :  40.9        table       :  14.3        
tvs         :  24.0        furniture   :  41.2        objects     :  26.5        
mIoU 0.40432 was not an improvement from 0.42617
ceil        :  27.9        floor       :  92.2        wall        :  34.6        window      :  25.8        
chair       :  28.4        bed         :  68.6        sofa        :  49.6        table       :  21.2        
tvs         :  24.2        furniture   :  43.1        objects     :  29.2        
mIoU improved from 0.42617 to  0.42689
ceil        :  42.6        floor       :  92.3        wall        :  37.0        window      :  23.8        
chair       :  34.1        bed         :  60.2        sofa        :  53.5        table       :  24.3        
tvs         :  27.0        furniture   :  43.6        objects     :  31.2        
mIoU 0.41219 was not an improvement from 0.42689
ceil        :  36.5        floor       :  92.0        wall        :  35.6        window      :  29.1        
chair       :  30.8        bed         :  68.1        sofa        :  44.0        table       :  18.7        
tvs         :  26.3        furniture   :  42.4        objects     :  30.0        
mIoU 0.41973 was not an improvement from 0.42689
ceil        :  37.3        floor       :  91.8        wall        :  40.5        window      :  24.2        
chair       :  31.5        bed         :  64.0        sofa        :  50.5        table       :  24.3        
tvs         :  24.8        furniture   :  43.2        objects     :  29.6        
mIoU 0.42421 was not an improvement from 0.42689
ceil        :  42.8        floor       :  92.0        wall        :  34.5        window      :  27.5        
chair       :  30.5        bed         :  61.6        sofa        :  52.1        table       :  25.7        
tvs         :  26.5        furniture   :  43.1        objects     :  30.4        
mIoU improved from 0.42689 to  0.43573
ceil        :  41.2        floor       :  92.1        wall        :  39.0        window      :  29.8        
chair       :  32.9        bed         :  68.9        sofa        :  51.7        table       :  23.6        
tvs         :  25.9        furniture   :  47.0        objects     :  27.1        
mIoU improved from 0.43573 to  0.45003
ceil        :  41.5        floor       :  92.0        wall        :  38.9        window      :  26.7        
chair       :  34.8        bed         :  70.1        sofa        :  54.7        table       :  26.3        
tvs         :  29.9        furniture   :  50.5        objects     :  29.7        
mIoU 0.42965 was not an improvement from 0.45003
ceil        :  35.4        floor       :  92.2        wall        :  37.1        window      :  29.9        
chair       :  34.8        bed         :  66.6        sofa        :  52.4        table       :  23.1        
tvs         :  27.4        furniture   :  43.9        objects     :  29.9        
mIoU 0.37476 was not an improvement from 0.45003
ceil        :  22.9        floor       :  91.9        wall        :  36.2        window      :  22.1        
chair       :  28.3        bed         :  56.9        sofa        :  45.1        table       :  12.1        
tvs         :  20.2        furniture   :  47.8        objects     :  28.7        
mIoU 0.43465 was not an improvement from 0.45003
ceil        :  40.8        floor       :  91.9        wall        :  34.2        window      :  29.4        
chair       :  34.5        bed         :  66.3        sofa        :  51.8        table       :  21.7        
tvs         :  31.3        furniture   :  45.9        objects     :  30.3        
mIoU 0.40407 was not an improvement from 0.45003
ceil        :  39.4        floor       :  91.9        wall        :  38.0        window      :  27.2        
chair       :  32.3        bed         :  57.3        sofa        :  46.5        table       :  17.4        
tvs         :  23.9        furniture   :  45.9        objects     :  24.7        
mIoU 0.41525 was not an improvement from 0.45003
ceil        :  39.6        floor       :  92.3        wall        :  34.5        window      :  25.3        
chair       :  34.8        bed         :  62.2        sofa        :  48.9        table       :  20.5        
tvs         :  26.0        furniture   :  46.9        objects     :  25.8        
mIoU 0.41854 was not an improvement from 0.45003
ceil        :  35.4        floor       :  92.4        wall        :  37.8        window      :  29.5        
chair       :  27.7        bed         :  68.6        sofa        :  52.2        table       :  24.0        
tvs         :  22.2        furniture   :  43.6        objects     :  27.0        
mIoU 0.39347 was not an improvement from 0.45003
ceil        :  42.2        floor       :  91.8        wall        :  39.3        window      :  28.4        
chair       :  26.5        bed         :  50.9        sofa        :  44.0        table       :  22.5        
tvs         :  27.2        furniture   :  32.7        objects     :  27.4        
mIoU 0.41828 was not an improvement from 0.45003
ceil        :  44.5        floor       :  92.2        wall        :  36.4        window      :  30.3        
chair       :  28.9        bed         :  58.9        sofa        :  48.6        table       :  24.4        
tvs         :  26.7        furniture   :  40.2        objects     :  29.0        
mIoU 0.39776 was not an improvement from 0.45003
ceil        :  33.6        floor       :  91.8        wall        :  41.0        window      :  28.6        
chair       :  20.3        bed         :  58.5        sofa        :  48.6        table       :  23.5        
tvs         :  24.5        furniture   :  39.1        objects     :  28.1        
mIoU 0.42878 was not an improvement from 0.45003
ceil        :  41.4        floor       :  92.1        wall        :  36.5        window      :  28.4        
chair       :  32.6        bed         :  66.4        sofa        :  48.4        table       :  21.4        
tvs         :  26.7        furniture   :  47.8        objects     :  30.0        
mIoU 0.41294 was not an improvement from 0.45003
ceil        :  38.6        floor       :  92.0        wall        :  38.5        window      :  25.3        
chair       :  33.2        bed         :  65.7        sofa        :  47.7        table       :  19.1        
tvs         :  26.6        furniture   :  40.3        objects     :  27.3        
mIoU 0.43049 was not an improvement from 0.45003
ceil        :  42.8        floor       :  91.6        wall        :  37.8        window      :  28.5        
chair       :  31.2        bed         :  68.2        sofa        :  49.3        table       :  22.1        
tvs         :  30.3        furniture   :  42.3        objects     :  29.5        
mIoU 0.43006 was not an improvement from 0.45003
ceil        :  41.7        floor       :  91.9        wall        :  37.6        window      :  29.7        
chair       :  32.1        bed         :  64.5        sofa        :  50.8        table       :  24.2        
tvs         :  27.8        furniture   :  42.9        objects     :  29.9        
mIoU 0.39941 was not an improvement from 0.45003
ceil        :  30.5        floor       :  92.0        wall        :  39.7        window      :  21.4        
chair       :  32.6        bed         :  57.3        sofa        :  45.2        table       :  18.3        
tvs         :  23.6        furniture   :  49.7        objects     :  29.1        
mIoU 0.43584 was not an improvement from 0.45003
ceil        :  42.0        floor       :  92.0        wall        :  40.2        window      :  30.9        
chair       :  31.1        bed         :  64.5        sofa        :  52.0        table       :  25.3        
tvs         :  26.6        furniture   :  47.0        objects     :  28.0        
mIoU 0.42966 was not an improvement from 0.45003
ceil        :  42.0        floor       :  91.9        wall        :  40.0        window      :  30.0        
chair       :  33.9        bed         :  61.2        sofa        :  50.6        table       :  22.9        
tvs         :  25.5        furniture   :  46.3        objects     :  28.3        
mIoU 0.42980 was not an improvement from 0.45003
ceil        :  43.0        floor       :  92.0        wall        :  35.9        window      :  28.6        
chair       :  32.7        bed         :  65.7        sofa        :  53.0        table       :  21.3        
tvs         :  25.4        furniture   :  45.5        objects     :  29.5        
mIoU 0.43597 was not an improvement from 0.45003
ceil        :  42.9        floor       :  92.0        wall        :  38.9        window      :  28.0        
chair       :  32.9        bed         :  66.9        sofa        :  52.1        table       :  23.8        
tvs         :  27.4        furniture   :  44.3        objects     :  30.3        
mIoU 0.42752 was not an improvement from 0.45003
ceil        :  42.2        floor       :  91.9        wall        :  36.2        window      :  28.7        
chair       :  30.4        bed         :  65.6        sofa        :  49.3        table       :  21.7        
tvs         :  31.9        furniture   :  42.7        objects     :  29.7        
mIoU 0.43233 was not an improvement from 0.45003
ceil        :  45.8        floor       :  91.8        wall        :  38.9        window      :  28.8        
chair       :  32.3        bed         :  64.9        sofa        :  49.7        table       :  21.9        
tvs         :  29.6        furniture   :  41.7        objects     :  30.3        
mIoU 0.43080 was not an improvement from 0.45003
ceil        :  44.8        floor       :  91.8        wall        :  39.2        window      :  28.6        
chair       :  32.4        bed         :  64.7        sofa        :  48.5        table       :  22.0        
tvs         :  28.9        furniture   :  44.3        objects     :  28.6        
mIoU 0.42900 was not an improvement from 0.45003
ceil        :  43.6        floor       :  91.9        wall        :  39.4        window      :  28.1        
chair       :  30.5        bed         :  63.6        sofa        :  49.3        table       :  23.0        
tvs         :  28.6        furniture   :  44.8        objects     :  29.1        
mIoU 0.43040 was not an improvement from 0.45003
ceil        :  43.4        floor       :  91.8        wall        :  38.6        window      :  27.9        
chair       :  31.2        bed         :  66.3        sofa        :  50.3        table       :  22.6        
tvs         :  28.4        furniture   :  44.0        objects     :  28.9        
mIoU 0.43272 was not an improvement from 0.45003
ceil        :  43.5        floor       :  91.6        wall        :  39.6        window      :  29.0        
chair       :  32.1        bed         :  65.9        sofa        :  49.7        table       :  22.7        
tvs         :  28.8        furniture   :  44.6        objects     :  28.6        
mIoU 0.42947 was not an improvement from 0.45003
ceil        :  45.3        floor       :  91.8        wall        :  39.7        window      :  30.1        
chair       :  32.3        bed         :  62.6        sofa        :  48.8        table       :  21.6        
tvs         :  28.0        furniture   :  43.1        objects     :  29.1        
mIoU 0.43330 was not an improvement from 0.45003
ceil        :  44.6        floor       :  91.9        wall        :  38.7        window      :  29.2        
chair       :  32.8        bed         :  64.4        sofa        :  50.3        table       :  21.6        
tvs         :  29.4        furniture   :  44.3        objects     :  29.5        
mIoU 0.43165 was not an improvement from 0.45003
ceil        :  45.9        floor       :  92.0        wall        :  39.5        window      :  29.5        
chair       :  32.5        bed         :  63.3        sofa        :  48.0        table       :  21.9        
tvs         :  28.7        furniture   :  44.8        objects     :  28.9        
mIoU 0.43360 was not an improvement from 0.45003
ceil        :  44.5        floor       :  91.9        wall        :  39.5        window      :  29.8        
chair       :  32.8        bed         :  65.0        sofa        :  50.2        table       :  21.9        
tvs         :  27.9        furniture   :  44.9        objects     :  28.6        
mIoU 0.43187 was not an improvement from 0.45003
ceil        :  45.4        floor       :  91.8        wall        :  39.2        window      :  28.6        
chair       :  32.9        bed         :  64.9        sofa        :  49.9        table       :  21.7        
tvs         :  28.0        furniture   :  44.2        objects     :  28.5        
mIoU 0.43421 was not an improvement from 0.45003
ceil        :  43.9        floor       :  91.8        wall        :  39.2        window      :  28.5        
chair       :  31.8        bed         :  66.4        sofa        :  50.5        table       :  22.3        
tvs         :  29.2        furniture   :  44.8        objects     :  29.2        
mIoU 0.43248 was not an improvement from 0.45003
ceil        :  44.8        floor       :  91.8        wall        :  39.2        window      :  28.7        
chair       :  31.9        bed         :  65.0        sofa        :  50.1        table       :  22.0        
tvs         :  29.0        furniture   :  44.5        objects     :  28.7        
mIoU 0.43397 was not an improvement from 0.45003
ceil        :  45.1        floor       :  91.8        wall        :  39.5        window      :  29.0        
chair       :  32.0        bed         :  65.3        sofa        :  49.9        table       :  21.8        
tvs         :  28.9        furniture   :  45.0        objects     :  29.1        
mIoU 0.43005 was not an improvement from 0.45003
ceil        :  44.2        floor       :  91.8        wall        :  39.1        window      :  28.7        
chair       :  31.2        bed         :  65.4        sofa        :  49.5        table       :  21.3        
tvs         :  29.0        furniture   :  44.2        objects     :  28.8        
mIoU 0.43178 was not an improvement from 0.45003
ceil        :  44.5        floor       :  91.8        wall        :  38.8        window      :  29.0        
chair       :  31.4        bed         :  65.4        sofa        :  50.0        table       :  21.5        
tvs         :  29.0        furniture   :  44.3        objects     :  29.1        
mIoU 0.43170 was not an improvement from 0.45003
ceil        :  44.8        floor       :  91.8        wall        :  39.0        window      :  28.6        
chair       :  32.1        bed         :  65.2        sofa        :  49.9        table       :  21.5        
tvs         :  28.5        furniture   :  44.9        objects     :  28.6        
R294_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.7-Int-3.0
Training complete in 276m 26s
Best val MIoU%:   45.0  Epoch: 44
ceil        :  41.5        floor       :  92.0        wall        :  38.9        window      :  26.7        
chair       :  34.8        bed         :  70.1        sofa        :  54.7        table       :  26.3        
tvs         :  29.9        furniture   :  50.5        objects     :  29.7        
