#include <fstream>

//nvcc --ptxas-options=-v --compiler-options '-fPIC' cuda_test.cu -o cuda_test

__global__ void cuda_hello(){
    printf("Hello World from GPU!\n");
}

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert (Error %d): %s File: %s Line: %d\n", code, cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}


int main(int argc, char *argv[]) {

    int device = 0;
    if (argc == 2)
       device = atoi(argv[1]);

    cudaDeviceProp deviceProperties;
    gpuErrchk(cudaGetDeviceProperties(&deviceProperties, device));
    cudaSetDevice(0);

    printf("Using GPU: %s - (device %d)\n", deviceProperties.name, device);

    cuda_hello<<<1,1>>>();

    gpuErrchk( cudaPeekAtLastError() );
    gpuErrchk( cudaDeviceSynchronize() );

    printf("Bye, bye!\n");

    return 0;
}
