import argparse
import os
from cuda import normals
from cuda import preproc3d
from utils import path_config
from utils.data import get_file_prefixes_from_path
from utils.image import get_class_colors
from tqdm import tqdm
import sys
from skimage import io




##################################
# Parameters
##################################

path_dict = path_config.read_config()
TRAIN_TEST = 'test'
DATASET = 'NYU'
SUFFIX = ""
OUT_PATH = ""
DEVICE = 0

class_colors = get_class_colors()


def parse_arguments():
    global OUT_PATH, DATASET, TRAIN_TEST, SUFFIX, DEVICE

    print("\nSemantic Scene Completion Surface Normals Calculation Script\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset",       help="SUNCG, NYU or NYUCAD?", type=str, choices=['SUNCG', 'NYU','NYUCAD'])
    parser.add_argument("set",           help="Train or test?", type=str, choices=['train', 'test'])
    parser.add_argument("--suffix",      help="Search suffix. Default " + str(SUFFIX),
                                         type=str, default=SUFFIX, required=False)
    parser.add_argument("--device",      help="Device. Default " + str(DEVICE),
                                         type=int, default=DEVICE, required=False)
    args = parser.parse_args()

    SUFFIX = args.suffix
    TRAIN_TEST = args.set
    DATASET = args.dataset
    DEVICE = args.device

    if DATASET == "SUNCG":
        if TRAIN_TEST == "train":
            OUT_PATH = path_dict["SUNCG_BASE_TRAIN"]
        else:
            OUT_PATH = path_dict["SUNCG_BASE_TEST"]
    elif DATASET == "NYU":
        if TRAIN_TEST == "train":
            OUT_PATH = path_dict["NYU_BASE_TRAIN"]
        else:
            OUT_PATH = path_dict["NYU_BASE_TEST"]
    else:
        if TRAIN_TEST == "train":
            OUT_PATH = path_dict["NYUCAD_BASE_TRAIN"]
        else:
            OUT_PATH = path_dict["NYUCAD_BASE_TEST"]


def process_scene(image_prefix):
    #image_prefix = '/d02/data/NYU_V2/NYUtest/NYU0009_0000'
    #image_prefix = '/d02/data/suncg_out/test/e5/e5c91fa85ebb60073d89c6ed56e66593_fl000_rm0000_000000'

    #vox_tsdf, vox_edges, tsdf_edges, segmentation_label, vox_weights, vox_vol = py_cuda.process_edges(image_prefix,
    #                                                                                                  voxel_shape=(240, 144, 240),
    #                                                                                                  down_scale=4)

    #print(image_prefix)


    import numpy as np
    #print(image_prefix)
    normals_image, labels_2d = normals.get_normals(image_prefix, (240,144,240))

    #print("n", normals_image.shape)
    #print("nu",np.unique(normals_image))
    #print("l",labels_2d.shape)
    #print("lu",np.unique(labels_2d))

    io.imsave(image_prefix + "_normals.png", normals_image)
    if DATASET == 'NYUCAD':

        io.imsave(image_prefix + "_labels.png", labels_2d)
        shifted_labels = labels_2d - 1
        shifted_labels[shifted_labels==255] = 11
        labels_2d_rgb = class_colors[shifted_labels]
        #print("lr",labels_2d_rgb.shape)
        io.imsave(image_prefix + "_labels_rgb.png", labels_2d_rgb)


def process():
    floor_high = 4.0 if DATASET == "NYU" else 0.0

    preproc3d.lib_preproc_setup(device=DEVICE, num_threads=128, K=None, frame_shape=(640, 480),
                                v_unit=0.02, v_margin=0.24, debug=0, floor_high=floor_high)

    print("DATASET:", DATASET)
    print("SET:", TRAIN_TEST)
    print("SUFFIX:", SUFFIX)
    print("OUT_PATH:", OUT_PATH)

    file_prefixes = get_file_prefixes_from_path(OUT_PATH, criteria='*.bin')
    print("Total files:", len(file_prefixes))

    if SUFFIX != "none" and SUFFIX != "":
        to_process = []
        for file_prefix in file_prefixes:
            base_name = os.path.basename(file_prefix)
            if base_name[:len(SUFFIX)] != SUFFIX:
                continue
            to_process.append(file_prefix)
    else:
        to_process = file_prefixes

    for file_prefix in tqdm(to_process, file=sys.stdout):

        process_scene(file_prefix)



# Main Function
def Run():
    parse_arguments()
    process()


if __name__ == '__main__':
  Run()
