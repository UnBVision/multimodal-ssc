from cuda.preproc3d import lib_preproc_setup, process
from utils.data import get_file_prefixes_from_path
import numpy as np
import h5py
from utils.cuda import get_device
from tqdm import tqdm
import os
import time
from utils.metrics import MIoU
from torch import Tensor
import torch
from utils.obj import vox2obj, vol2obj, weights2obj

#dev = get_device("cuda:0")
nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
               "empty"]

vox_shape = (240, 144, 240)
vox_shape_down = (60, 36, 60)
batch_shape = (1,60, 36, 60)
batch_shape_ch = (1,60, 36, 60, 12)

obj_dir = "obj"

base_path={
        'train': "/d02/data/NYU_V2/NYUtrain",
        'valid': "/d02/data/NYU_V2/NYUtest"
}

#prefixes = {
#        'train': get_file_prefixes_from_path(base_path['train']),
#        'valid': get_file_prefixes_from_path(base_path['valid'])
#}


prefixes = ['/d02/data/NYU_V2/NYUtest/NYU0959_0000',
            '/d02/data/NYU_V2/NYUtest/NYU1202_0000',
            '/d02/data/NYU_V2/NYUtest/NYU1314_0000',
            '/d02/data/NYU_V2/NYUtest/NYU0638_0000',
            '/d02/data/NYU_V2/NYUtest/NYU0524_0000']

vol_path = '/d02/data/sscnet/eval/NYUtest'
#v_unit = 0.0194
v_unit = 0.02

lib_preproc_setup(device=1, num_threads=128, K=None, frame_shape=(640, 480), v_unit=v_unit, v_margin=0.24, debug=0)

iou = MIoU(num_classes=12, ignore_class=0)

for dataset in ['valid']:
    #with tqdm(total=len(prefixes[dataset]), desc="") as pbar:
    with tqdm(total=len(prefixes), desc="") as pbar:
        #for i, prefix in enumerate(prefixes[dataset]):
        for i, prefix in enumerate(prefixes):
            start_time = time.time()

            vox_grid, vox_tsdf, vox_prior, segmentation_label, vox_weights = process(prefix, vox_shape)
            #print("preproc_test-lib call: %6.4f seconds" % (time.time() - start_time))

            basename = os.path.basename(prefix)

            start_time = time.time()
            #iou.update(Tensor(np.moveaxis(vox_prior.reshape(batch_shape_ch), -1,1)),
            #           Tensor(segmentation_label.reshape(batch_shape)),
            #           vol=Tensor(vol.reshape(batch_shape)))
            iou.update(Tensor(np.moveaxis(vox_prior.reshape(batch_shape_ch), -1,1)),
                       Tensor(segmentation_label.reshape(batch_shape)),
                       weights=Tensor(vox_weights.reshape(batch_shape)))



            surface_file = os.path.join(obj_dir,basename) + "_surf"
            vox2obj(surface_file, vox_grid, vox_shape, 0.02, include_top=True, triangular=False, inner_faces=True,
                    complete_walls=False, colors=None, gap=0.15)
            #print("preproc_test-obj call1: %6.4f seconds" % (time.time() - start_time))

            gt_file = os.path.join(obj_dir,basename) + "_gt"
            vox2obj(gt_file, segmentation_label, vox_shape_down, 0.08, include_top=True, triangular=False, inner_faces=True,
                    complete_walls=False, colors=None, gap=0.15)
            #print("preproc_test-obj call2: %6.4f seconds" % (time.time() - start_time))

            #print("vox_prior", vox_prior.shape)

            prior_file = os.path.join(obj_dir,basename) + "_prior"
            vox2obj(prior_file, np.argmax(vox_prior,-1), vox_shape_down, 0.08, include_top=True, triangular=False, inner_faces=True,
                    complete_walls=False, colors=None, gap=0.15)
            #print("preproc_test-obj call2: %6.4f seconds" % (time.time() - start_time))

            weights_file = os.path.join(obj_dir,basename) + "_weights"
             #print("preproc_test-obj call3: %6.4f seconds" % (time.time() - start_time))

            vol_file = os.path.join(obj_dir,basename) + "_vol"
            f = h5py.File(os.path.join(vol_path, basename + '_vol_d4.mat'),'r')
            vol = np.array(f['flipVol_ds']).reshape(vox_shape_down)

            #vol = vol.view(list(vox_weights.size()))

            #vox_weights[vol<-1] = 0 # Outside room as defined by SSCNET Song et. al. 2017
            #vox_weights[(vox_weights > 1) & (vol < -1)] = 0

            weights2obj(weights_file, vox_weights, vox_shape_down, 0.08, gap=0.15)
            vol2obj(vol_file, vol, vox_shape_down, 0.08, gap=0.15)
            #print("preproc_test-obj call3: %6.4f seconds" % (time.time() - start_time))

            #print("vox_grid", np.unique(vox_grid))
            #print("vox_weights", np.unique(vox_weights))
            #print("label_grid", np.unique(segmentation_label), segmentation_label.shape)


            pbar.set_description('{} - miou={:5.1f}'.format(prefix, iou.compute()*100))
            #print("iuo update: %6.4f seconds" % (time.time() - start_time))
            pbar.update()
            if i == 5:
                break

    per_class_iou = iou.per_class_iou()
    for i in range(len(per_class_iou)):
        text = '{:12.12}: {:5.1f}'.format(nyu_classes[i], 100 * per_class_iou[i])
        print(text, end="        ")
        if i % 4 == 3:
            print()
