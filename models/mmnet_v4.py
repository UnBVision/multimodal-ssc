import torch
import torch.nn as nn
import torch.nn.functional as F
from collections import OrderedDict


class MSDDRUnit3D(nn.Module):
    def __init__(self, c_in, c, c_out, kernel=3, stride=1, max_dilation=1, residual=True, batch_norm=False):
        super(MSDDRUnit3D, self).__init__()
        s = stride
        k = kernel
        self.batch_norm = batch_norm
        self.conv_in = nn.Conv3d(c_in, c, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm3d(c) if batch_norm else None

        self.conv1x1x3 = nn.ModuleList()
        self.conv1x3x1 = nn.ModuleList()
        self.conv3x1x1 = nn.ModuleList()

        for rd in range(max_dilation):
            d = rd + 1

            p = k // 2 * (d)

            self.conv1x1x3.append(
                nn.Conv3d(c, c, (1, 1, k), stride=s, padding=(0, 0, p), bias=True, dilation=(1, 1, d))
            )
            self.conv1x3x1.append(
                nn.Conv3d(c, c, (1, k, 1), stride=s, padding=(0, p, 0), bias=True, dilation=(1, d, 1))
            )
            self.conv3x1x1.append(
                nn.Conv3d(c, c, (k, 1, 1), stride=s, padding=(p, 0, 0), bias=True, dilation=(d, 1, 1))
            )
        self.bn4 = nn.BatchNorm3d(c) if batch_norm else None
        self.conv_out = nn.Conv3d(c*max_dilation, c_out, kernel_size=1, bias=False)
        self.bn5 = nn.BatchNorm3d(c_out) if batch_norm else None
        self.residual = residual
        self.conv_resid = nn.Conv3d(c_in, c_out, kernel_size=1, bias=False) if residual and c_in != c_out else None


    def forward(self, x):
        y0 = self.conv_in(x)
        if self.batch_norm:
            y0 = self.bn1(y0)
        y0 = F.relu(y0, inplace=True)

        out = []

        for conv1x1x3, conv1x3x1, conv3x1x1 in zip(self.conv1x1x3, self.conv1x3x1, self.conv3x1x1):

            y1 = conv1x1x3(y0)
            y1 = F.relu(y1, inplace=True)

            y2 = conv1x3x1(y1) + y1
            y2 = F.relu(y2, inplace=True)

            y3 = conv3x1x1(y2) + y2 + y1
            if self.batch_norm:
                y3 = self.bn4(y3)
            y3 = F.relu(y3, inplace=True)

            out.append(y3)

        y3 = torch.cat(out, dim=1)

        y = self.conv_out(y3)
        if self.batch_norm:
            y = self.bn5(y)

        x_squip = x if self.conv_resid is None else self.conv_resid(x)

        y = F.relu(y + x_squip, inplace=True) if self.residual else F.relu(y, inplace=True)
        return y


class MSDDRBlock3D(nn.Module):
    def __init__(self, c_in, c, c_out, units=2, kernel=3, stride=1, dilation=1, pool=True, residual=True, batch_norm=False):
        super(MSDDRBlock3D, self).__init__()
        self.pool = nn.MaxPool3d(2, stride=2) if pool else None
        self.units = nn.ModuleList()
        for i in range(units):
            if i == 0:
                self.units.append(MSDDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm))
            else:
                self.units.append(MSDDRUnit3D(c_out, c, c_out, kernel, stride, dilation, residual, batch_norm))

    def forward(self, x):
        y = self.pool(x) if self.pool is not None else x
        for ddr_unit in self.units:
            y = ddr_unit(y)
        return y

class MSDDRBlock3DUP(nn.Module):
    def __init__(self, c_in, c, c_out, units=2, kernel=3, stride=1, dilation=1, residual=True,
                 batch_norm=False):
        super(MSDDRBlock3DUP, self).__init__()
        self.transp = nn.ConvTranspose3d(c_in, c_out, kernel_size=2, stride=2)
        self.units = nn.ModuleList()
        for i in range(units):
           self.units.append(MSDDRUnit3D(c_out, c, c_out, kernel, stride, dilation, residual, batch_norm))

    def forward(self, x):
        y = self.transp(x)
        for ddr_unit in self.units:
            y = ddr_unit(y)
        return y

class MMNet(nn.Module):

    def __init__(self, residual=True, batch_norm=True):
        super(MMNet, self).__init__()
        # depth branch
        self.d1 = nn.Conv3d(1, 8, 3, stride=1, bias=True, padding=1)
        self.d2 = MSDDRBlock3D(8, 16, 16, units=1, residual=residual, batch_norm=batch_norm, pool=True)
        self.d_out = MSDDRBlock3D(16, 32, 32, units=1, residual=residual, batch_norm=batch_norm, pool=True)

        # 2D priors branch
        self.p1 = nn.Conv3d(12, 16, 3, stride=1, bias=True, padding=1)
        self.p2 = MSDDRBlock3D(16, 16, 16, units=1, pool=False, residual=residual, batch_norm=batch_norm)
        self.p_out = MSDDRBlock3D(16, 16, 32, units=1, pool=False, residual=residual, batch_norm=batch_norm)

        # encoder
        self.enc1 = MSDDRBlock3D(32, 64, 64, units=2, dilation=2, residual=residual, batch_norm=batch_norm, pool=True)
        self.enc2 = MSDDRBlock3D(64, 128, 128, units=2, dilation=3, residual=residual, batch_norm=batch_norm, pool=True)

        # decoder
        self.dec2 = MSDDRBlock3DUP(128, 64, 64, units=2, dilation=2, residual=residual, batch_norm=batch_norm)
        self.dec1 = MSDDRBlock3DUP(64, 32, 32, units=2, dilation=1, residual=residual, batch_norm=batch_norm)


        # final
        self.fd1 = MSDDRBlock3D(32, 16, 16, units=1, pool=False, residual=residual, batch_norm=batch_norm)
        self.fd2 = nn.Conv3d(16, 12, 3, stride=1, bias=True, padding=1)


    def forward(self, depth, priors):

        # get outputs from encoder
        #print("depth")
        d = self.d1(depth)
        #print(d.shape)
        d = self.d2(d)
        #print(d.shape)
        d_out = self.d_out(d)

        #print("d_out", d_out.shape)

        #print("priors")
        #priors = self.softmax_priors(priors)
        p = self.p1(priors)
        #print(p.shape)
        p = self.p2(p)
        #print(p.shape)
        p_out = self.p_out(p)

        #print("p_out", p_out.shape)
        #print("encoder")
        e1 = self.enc1(d_out+p_out)
        #print("e1", e1.shape)
        e2 = self.enc2(e1)
        #print("e2", e2.shape)

        #print("decoder")
        d2 = self.dec2(e2)
        #print("d2", d2.shape)
        d1 = self.dec1(e1 + d2)

        #final
        f = self.fd1(d1 + d_out + p_out)
        f = self.fd2(f)

        return f
'''
class MMNetDepth(nn.Module):

    def __init__(self, residual=True, batch_norm=True):
        super(MMNetDepth, self).__init__()
        # depth branch
        self.d1 = nn.Conv3d(1, 8, 3, stride=1, bias=True, padding=1)
        self.d2 = DDRBlock3D(8, 16, 16, units=1, residual=residual, batch_norm=batch_norm, pool=True)
        self.d_out = DDRBlock3D(16, 32, 32, units=1, residual=residual, batch_norm=batch_norm, pool=True)

        # encoder
        self.enc1 = DDRBlock3D(32, 64, 64, units=2, dilation=2, residual=residual, batch_norm=batch_norm, pool=True)
        self.enc2 = DDRBlock3D(64, 128, 128, units=2, dilation=3, residual=residual, batch_norm=batch_norm, pool=True)

        # decoder
        self.dec2 = DDRBlock3DUP(128, 64, 64, units=2, dilation=3, residual=residual, batch_norm=batch_norm)
        self.dec1 = DDRBlock3DUP(64, 32, 32, units=2, dilation=2, residual=residual, batch_norm=batch_norm)


        # final
        self.fd1 = DDRBlock3D(32, 16, 16, units=1, pool=False, residual=residual, batch_norm=batch_norm)
        self.fd2 = nn.Conv3d(16, 12, 3, stride=1, bias=True, padding=1)

    def forward(self, depth, priors):

        # get outputs from encoder
        #print("depth")
        d = self.d1(depth)
        #print(d.shape)
        d = self.d2(d)
        #print(d.shape)
        d_out = self.d_out(d)

        #print("d_out", d_out.shape)


        #print("p_out", p_out.shape)
        #print("encoder")
        e1 = self.enc1(d_out)
        #print("e1", e1.shape)
        e2 = self.enc2(e1)
        #print("e2", e2.shape)

        #print("decoder")
        d2 = self.dec2(e2)
        #print("d2", d2.shape)
        d1 = self.dec1(e1)

        #final
        f = self.fd1(d1 + d_out)
        f = self.fd2(f)

        return f

class MMNetNoDepth(nn.Module):

    def __init__(self, residual=True, batch_norm=True, sgr=True):
        super(MMNetNoDepth, self).__init__()

        # 2D priors branch
        self.p1 = nn.Conv3d(12, 16, 3, stride=1, bias=True, padding=1)
        self.p2 = DDRBlock3D(16, 16, 16, units=1, pool=False, residual=residual, batch_norm=batch_norm)
        self.p_out = DDRBlock3D(16, 16, 32, units=1, pool=False, residual=residual, batch_norm=batch_norm)

        # encoder
        self.enc1 = DDRBlock3D(32, 64, 64, units=2, dilation=2, residual=residual, batch_norm=batch_norm, pool=True)
        self.enc2 = DDRBlock3D(64, 128, 128, units=2, dilation=3, residual=residual, batch_norm=batch_norm, pool=True)

        # decoder
        self.dec2 = DDRBlock3DUP(128, 64, 64, units=2, dilation=3, residual=residual, batch_norm=batch_norm)
        self.dec1 = DDRBlock3DUP(64, 32, 32, units=2, dilation=2, residual=residual, batch_norm=batch_norm)


        # final
        self.fd1 = DDRBlock3D(32, 16, 16, units=1, pool=False, residual=residual, batch_norm=batch_norm)
        self.fd2 = nn.Conv3d(16, 12, 3, stride=1, bias=True, padding=1)


    def forward(self, depth, priors):

        p = self.p1(priors)
        #print(p.shape)
        p = self.p2(p)
        #print(p.shape)
        p_out = self.p_out(p)

        e1 = self.enc1(p_out)
        #print("e1", e1.shape)
        e2 = self.enc2(e1)
        #print("e2", e2.shape)

        #print("decoder")
        d2 = self.dec2(e2)
        #print("d2", d2.shape)
        d1 = self.dec1(e1 + d2)

        #final
        f = self.fd1(d1 + p_out)
        f = self.fd2(f)

        return f

'''
def get_mmnet(sgr=False, batch_norm=True):

    model = MMNet(residual=True, batch_norm=batch_norm)

    return model
'''
def get_mmnet_depth(batch_norm=True):

    model = MMNetDepth(residual=True, batch_norm=batch_norm)

    return model

def get_mmnet_no_depth(batch_norm=True):

    model = MMNetNoDepth(residual=True, batch_norm=batch_norm)

    return model
'''