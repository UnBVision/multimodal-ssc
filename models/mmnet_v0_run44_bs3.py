import torch
import torch.nn as nn
import torch.nn.functional as F
from collections import OrderedDict


class DDRUnit3D(nn.Module):
    def __init__(self, c_in, c, c_out, kernel=3, stride=1, dilation=1, residual=True, batch_norm=False):
        super(DDRUnit3D, self).__init__()
        s = stride
        k = kernel
        d = dilation
        p = k // 2 * d
        self.batch_norm = batch_norm
        self.conv_in = nn.Conv3d(c_in, c, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm3d(c) if batch_norm else None
        self.conv1x1x3 = nn.Conv3d(c, c, (1, 1, k), stride=s, padding=(0, 0, p), bias=True, dilation=(1, 1, d))
        self.bn2 = nn.BatchNorm3d(c) if batch_norm else None
        self.conv1x3x1 = nn.Conv3d(c, c, (1, k, 1), stride=s, padding=(0, p, 0), bias=True, dilation=(1, d, 1))
        self.bn3 = nn.BatchNorm3d(c) if batch_norm else None
        self.conv3x1x1 = nn.Conv3d(c, c, (k, 1, 1), stride=s, padding=(p, 0, 0), bias=True, dilation=(d, 1, 1))
        self.bn4 = nn.BatchNorm3d(c) if batch_norm else None
        self.conv_out = nn.Conv3d(c, c_out, kernel_size=1, bias=False)
        self.bn5 = nn.BatchNorm3d(c_out) if batch_norm else None
        self.residual = residual
        self.conv_resid = nn.Conv3d(c_in, c_out, kernel_size=1, bias=False) if residual and c_in != c_out else None


    def forward(self, x):
        y0 = self.conv_in(x)
        if self.batch_norm:
            y0 = self.bn1(y0)
        y0 = F.relu(y0, inplace=True)

        y1 = self.conv1x1x3(y0)
        if self.batch_norm:
            y1 = self.bn2(y1)
        y1 = F.relu(y1, inplace=True)

        y2 = self.conv1x3x1(y1) + y1
        if self.batch_norm:
            y2 = self.bn3(y2)
        y2 = F.relu(y2, inplace=True)

        y3 = self.conv3x1x1(y2) + y2 + y1
        if self.batch_norm:
            y3 = self.bn4(y3)
        y3 = F.relu(y3, inplace=True)

        y = self.conv_out(y3)
        if self.batch_norm:
            y = self.bn5(y)

        x_squip = x if self.conv_resid is None else self.conv_resid(x)

        y = F.relu(y + x_squip, inplace=True) if self.residual else F.relu(y, inplace=True)
        return y


class DDRBlock3D(nn.Module):
    def __init__(self, c_in, c, c_out, units=2, kernel=3, stride=1, dilation=1, pool=True, residual=True, batch_norm=False):
        super(DDRBlock3D, self).__init__()
        self.pool = nn.MaxPool3d(2, stride=2) if pool else None
        self.units = nn.ModuleList()
        for i in range(units):
            if i == 0:
                self.units.append(DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm))
            else:
                self.units.append(DDRUnit3D(c_out, c, c_out, kernel, stride, dilation, residual, batch_norm))

    def forward(self, x):
        y = self.pool(x) if self.pool is not None else x
        for ddr_unit in self.units:
            y = ddr_unit(y)
        return y


class MMFUnit3D(nn.Module):
    def __init__(self, c_in, c, c_out, kernel=3, stride=1, dilation=1, residual=True, batch_norm=False):
        super(MMFUnit3D, self).__init__()
        self.ddr1_inp1 = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)
        #self.ddr2_inp1 = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)
        self.ddr1_inp2 = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)
        #self.ddr2_inp2 = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)
        self.ddr_out = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)

    def forward(self, x1, x2):
        y1 = self.ddr1_inp1(x1)
        #y1 = self.ddr2_inp1(y1)

        y2 = self.ddr1_inp2(x2)
        #y2 = self.ddr2_inp2(y2)

        y = self.ddr_out(y1+y2)
        return y


class MSFUnit3D(nn.Module):
    def __init__(self, c_in, c, c_out, c_low, kernel=3, stride=1, dilation=1, residual=True, batch_norm=False):
        super(MSFUnit3D, self).__init__()
        self.transpose = nn.ConvTranspose3d(c_low, c_in, kernel_size=2, stride=2)
        self.ddr1_inp1 = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)
        #self.ddr2_inp1 = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)
        self.ddr1_inp2 = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)
        #self.ddr2_inp2 = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)
        self.ddr_out = DDRUnit3D(c_in, c, c_out, kernel, stride, dilation, residual, batch_norm)

    def forward(self, x1, x2):
        #print("x1",x1.shape)
        y1 = self.transpose(x1)
        #print("y1",y1.shape)
        y1 = self.ddr1_inp1(y1)
        #print("y1",y1.shape)
        #y1 = self.ddr2_inp1(y1)

        y2 = self.ddr1_inp2(x2)
        #print("y2",y2.shape)
        #y2 = self.ddr2_inp2(y2)

        y = self.ddr_out(y1+y2)
        return y


class MMNet(nn.Module):

    def __init__(self, residual=True, batch_norm=True):
        super(MMNet, self).__init__()

        # depth branch
        self.depth1 = DDRBlock3D(1, 4, 8, units=1, pool=False, residual=residual, batch_norm=batch_norm)
        self.depth2 = DDRBlock3D(8, 4, 8, units=1, residual=residual, batch_norm=batch_norm)
        self.depth3 = DDRBlock3D(8, 8, 16, units=1, residual=residual, batch_norm=batch_norm)
        self.depth4 = DDRBlock3D(16, 16, 32, units=2, dilation=2, residual=residual, batch_norm=batch_norm)
        self.depth5 = DDRBlock3D(32, 32, 64, units=2, dilation=2, residual=residual, batch_norm=batch_norm)

        # 2D priors branch
        self.prior3 = DDRBlock3D(12, 8, 16, units=1, pool=False, residual=residual, batch_norm=batch_norm)
        self.prior4 = DDRBlock3D(16, 16, 32, units=2, dilation=2, residual=residual, batch_norm=batch_norm)
        self.prior5 = DDRBlock3D(32, 32, 64, units=2, dilation=2, residual=residual, batch_norm=batch_norm)

        # mmf modules
        self.mmf1 = MMFUnit3D(64, 32, 64, kernel=3, stride=1, dilation=2, residual=residual, batch_norm=batch_norm)
        self.mmf2 = MMFUnit3D(32, 16, 32, kernel=3, stride=1, dilation=2, residual=residual, batch_norm=batch_norm)
        self.mmf3 = MMFUnit3D(16, 8, 16, kernel=3, stride=1, residual=residual, batch_norm=batch_norm)
        self.mmf4 = MMFUnit3D(12, 12, 12, kernel=3, stride=1, residual=residual, batch_norm=batch_norm)

        # msf modules
        self.msf1 = MSFUnit3D(32, 16, 32, 64, kernel=3, stride=1, dilation=2, residual=residual, batch_norm=batch_norm)
        self.msf2 = MSFUnit3D(16, 8, 16, 32, kernel=3, stride=1, dilation=1, residual=residual, batch_norm=batch_norm)

        # guidance ddr
        self.guide_ddr = DDRUnit3D(16, 8, 12, kernel=3, stride=1, dilation=1, residual=residual, batch_norm=batch_norm)

        # final
        self.final = DDRBlock3D(12, 12, 12, units=2, pool=False, residual=residual, batch_norm=batch_norm)

    def forward(self, depth, priors):
        # get outputs from encoder
        #print("depth")
        d1 = self.depth1(depth)
        d2 = self.depth2(d1)
        d3 = self.depth3(d2)
        d4 = self.depth4(d3)
        d5 = self.depth5(d4)

        #print("priors")
        p3 = self.prior3(priors)
        p4 = self.prior4(p3)
        p5 = self.prior5(p4)

        #print("mmf")
        m1 = self.mmf1(d5, p5)
        m2 = self.mmf2(d4, p4)
        m3 = self.mmf3(d3, p3)

        #print("msf")
        #print(m1.shape, m2.shape)
        s1 = self.msf1(m1,m2)
        #print(s1.shape, m3.shape)
        s2 = self.msf2(s1,m3)

        #print("guide")
        g = self.guide_ddr(s2)

        #print("out")
        out = self.mmf4(g,priors)
        out = self.final(out)

        return out


def get_mmnet():

    model = MMNet(residual=True, batch_norm=False)

    return model
