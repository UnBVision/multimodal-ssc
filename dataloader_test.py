from tqdm import tqdm
import os

from utils.data import SSCMultimodalDataset, sample2dev
from utils.data import get_file_prefixes_from_path
from torch.utils.data import DataLoader
import torch
from utils.cuda import get_device
import numpy as np
from utils.metrics import MIoU
from utils.losses import weighted_categorical_crossentropy

BATCH_SIZE = 4
preproc_path = "/d01/data/mmssc_preproc_rgb_normals/NYU"

nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
               "empty"]

dev = get_device("cuda:1")

train_prefixes = get_file_prefixes_from_path(os.path.join(preproc_path, "train"), criteria="*.npz")
valid_prefixes = get_file_prefixes_from_path(os.path.join(preproc_path, "valid"), criteria="*.npz")

train_ds = SSCMultimodalDataset(train_prefixes)
valid_ds = SSCMultimodalDataset(valid_prefixes)

dataloaders = {
    'train': DataLoader(train_ds, batch_size=BATCH_SIZE, shuffle=True, num_workers=2),
    'valid': DataLoader(valid_ds, batch_size=BATCH_SIZE * 2, shuffle=False, num_workers=2)
}

vox_shape = (240, 144, 240)
vox_shape_down = (60, 36, 60)
batch_shape = (1,60, 36, 60)
batch_shape_ch = (1,60, 36, 60, 12)

iou = MIoU(num_classes=12, ignore_class=0)

for phase in [#'train',
              'valid']:

    running_loss = 0.0
    num_batches = 0

    with tqdm(total=len(dataloaders[phase]), desc="") as pbar:
        for sample in dataloaders[phase]:

            vox_tsdf = sample['vox_tsdf']
            vox_prior = sample['vox_prior']
            gt = sample['gt']
            vox_weights = sample['vox_weights']


            #print(sample['vox_prior'].shape, sample['gt'].shape, sample['vox_weights'].shape)

            sample = sample2dev(sample, dev)

            iou.update(vox_prior, gt, vox_weights)

            loss = weighted_categorical_crossentropy(vox_prior, gt, vox_weights)
            num_batches += 1
            running_loss += loss

            pbar.set_description('{} loss:{:7.5f} miou:{:5.1f}'.format(phase, running_loss/num_batches, iou.compute()*100))
            pbar.update()

    per_class_iou = iou.per_class_iou()
    for i in range(len(per_class_iou)):
        text = '{:12.12}: {:5.1f}'.format(nyu_classes[i], 100 * per_class_iou[i])
        print(text, end="        ")
        if i % 4 == 3:
            print()
