# Multimodal SSC

## Setup
Requirement: cuda 10.2 or later. When installing Pytorch, Specify cuda toolkit version accordingly.  

``` shell
    conda create -n torch python=3.8
    conda activate torch
    conda install -c anaconda jupyter
    conda install pytorch torchvision torchaudio cudatoolkit=11.0 -c pytorch
    conda install -c anaconda scikit-image 
    conda install -c conda-forge tqdm
    conda install -c conda-forge tensorboard
    conda install -c anaconda pandas
```



