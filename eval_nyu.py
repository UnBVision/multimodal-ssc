from tqdm import tqdm
import os

from utils.data import SSCMultimodalDataset, sample2dev
from utils.data import get_file_prefixes_from_path
from torch.utils.data import DataLoader
import torch
from utils.cuda import get_device
import numpy as np
from utils.metrics import MIoU, CompletionIoU
from models.mmnet import get_mmnet
import h5py

BATCH_SIZE = 1
preproc_path = "/d01/data/mmssc_preproc_rgb_normals/NYU"
vol_path = '/d02/data/sscnet/eval/NYUtest'

INPUT_TYPE = "depth+rgb+normals"
BATCH_NORM = True

nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
               "empty"]

dev = get_device("cuda:0")
#dev = torch.device("cpu")

valid_prefixes = get_file_prefixes_from_path(os.path.join(preproc_path, "valid"), criteria="*.npz")

valid_ds = SSCMultimodalDataset(valid_prefixes)

dataloader = DataLoader(valid_ds, batch_size=BATCH_SIZE, shuffle=False, num_workers=1)

vox_shape = (240, 144, 240)
vox_shape_down = (60, 36, 60)
batch_shape = (1,60, 36, 60)
batch_shape_ch = (1,60, 36, 60, 12)

miou = MIoU(num_classes=12, ignore_class=0)

ciou = CompletionIoU()

model = get_mmnet(input_type=INPUT_TYPE, batch_norm=BATCH_NORM, inst_norm=False)

#model.load_state_dict(torch.load("weights/R172_MMNet_D+RGB+NORM-BN-v8-NYU-Alp-0.6-Int-5_EPOCH_79"))
#model.load_state_dict(torch.load("weights/R179_MMNet_D+RGB+NORM-BN-v8-NYU-Alp-0-Int-7_EPOCH_79"))
model.load_state_dict(torch.load("weights/R172_MMNet_D+RGB+NORM-BN-v8-NYU-Alp-0.6-Int-5_EPOCH_79"))
model.to(dev)
model.eval()

torch.cuda.empty_cache()

for phase in ['valid']:

    running_loss = 0.0
    num_batches = 0

    with tqdm(total=len(dataloader), desc="") as pbar:
        for sample, prefix in zip(dataloader,valid_prefixes):

            sample = sample2dev(sample, dev)

            vox_tsdf = sample['vox_tsdf']
            vox_prior = sample['vox_prior']
            gt = sample['gt']
            vox_weights = sample['vox_weights']

            basename = os.path.basename(prefix)


            #sscnet partial evaluation
            f = h5py.File(os.path.join(vol_path, basename + '_vol_d4.mat'),'r')
            vol = np.array(f['flipVol_ds'])
            vol = torch.tensor((vol)).to(dev)
            vol = vol.view(list(vox_weights.size()))

            vox_weights[vol<-1] = 0 # Outside room as defined by SSCNET Song et. al. 2017

            pred = model(vox_tsdf, vox_prior)

            miou.update(pred, gt, vox_weights)
            ciou.update(pred, gt, vol=vol)

            pbar.set_description('{} miou:{:5.1f}'.format(phase, miou.compute()*100))
            pbar.update()

    comp_iou, precision, recall = ciou.compute()

    print("prec rec. IoU  MIou")
    print("{:4.1f} {:4.1f} {:4.1f} {:4.1f}".format( 100*precision, 100*recall, 100*comp_iou, miou.compute()*100))

    per_class_iou = miou.per_class_iou()
    for i in range(len(per_class_iou)):
        text = '{:12.12}: {:5.1f}'.format(nyu_classes[i], 100 * per_class_iou[i])
        print(text, end="        ")
        if i % 4 == 3:
            print()

    print("\nLatex Line:")
    print("{:4.1f} & {:4.1f} & {:4.1f} &".format( 100*precision, 100*recall, 100*comp_iou), end=" ")
    for i in range(len(per_class_iou)):
        text = '{:4.1f} &'.format(100 * per_class_iou[i])
        print(text, end=" ")

    print("{:4.1f} \\\\".format( miou.compute()*100))

