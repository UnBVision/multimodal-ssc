from cuda.preproc3d import lib_preproc_setup, process
from utils.data import get_file_prefixes_from_path
import numpy as np

from tqdm import tqdm
import os


vox_shape = (240, 144, 240)
vox_shape_down = (60, 36, 60)
batch_shape = (1,60, 36, 60)
batch_shape_ch = (1,60, 36, 60, 12)

nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
               "empty"]

preproc_path = "/d01/data/mmssc_preproc/NYU"

base_path={
        'train': "/d02/data/NYU_V2/NYUtrain",
        'valid': "/d02/data/NYU_V2/NYUtest"
}

prefixes = {
        'train': get_file_prefixes_from_path(base_path['train']),
        'valid': get_file_prefixes_from_path(base_path['valid'])
}

v_unit = 0.02

lib_preproc_setup(device=0, num_threads=128, K=None, frame_shape=(640, 480), v_unit=v_unit, v_margin=0.24, debug=0)


for dataset in ['train', 'valid']:
    with tqdm(total=len(prefixes[dataset]), desc="") as pbar:
        for i, prefix in enumerate(prefixes[dataset]):

            vox_grid, vox_tsdf, vox_prior, segmentation_label, vox_weights = process(prefix, vox_shape)

            basename = os.path.basename(prefix)
            preproc_dir = os.path.join(preproc_path, dataset)
            os.makedirs(preproc_dir,exist_ok=True)
            preproc_file = os.path.join(preproc_dir, basename+".npz")

            np.savez(preproc_file, vox_tsdf=vox_tsdf, vox_prior=vox_prior, gt=segmentation_label, vox_weights=vox_weights)

            pbar.set_description(prefix)
            pbar.update()
