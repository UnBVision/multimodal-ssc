import argparse
import os
from utils import path_config


# Dataloader settings
DATASET = "NYU"
FILE = ""
INPUT_TYPE = "rgb+normals"
obj_dir = "obj"
FINE_TUNE = False

vox_shape = (240, 144, 240)
vox_shape_down = (60, 36, 60)
batch_shape = (1, 60, 36, 60)
batch_shape_ch = (1, 60, 36, 60, 12)

nyu_classes = ["ceil", "floor", "wall", "window", "chair", "bed", "sofa", "table", "tvs", "furniture", "objects",
               "empty"]


def parse_arguments():
    global DATASET, FILE, INPUT_TYPE, FINE_TUNE

    print("\nMMNet OBJ visualization Script\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("dataset", help="Target dataset", type=str, choices=['SUNCG', 'NYU', 'NYUCAD'])
    parser.add_argument("train_valid", help="Target set", type=str, choices=['train', 'valid'])
    parser.add_argument("scene", help="Target scene", type=str)
    parser.add_argument("--input_type",  help="Network input type. Default " + INPUT_TYPE,
                        type=str, default=INPUT_TYPE, required=False,
                        choices=['rgb+normals', 'rgb']
                        )
    parser.add_argument("--fine_tune",  help="Is it a fine tune from a suncg model?. Default no",
                        type=str, default="no", required=False,
                        choices=['yes', 'Yes', 'y', 'Y', 'no', 'No', 'n', 'N']
                        )

    args = parser.parse_args()

    DATASET = args.dataset
    INPUT_TYPE = args.input_type
    SCENE = args.scene
    train_valid = args.train_valid
    FINE_TUNE = args.fine_tune in ['yes', 'Yes', 'y', 'Y']

    path_dict = path_config.read_config()

    if DATASET == "NYU":
        if not FINE_TUNE:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYU_RGB_PRIOR_FINE_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYU_RGB_NORMALS_FINE_PRIOR_PREPROC"]
        FILE = os.path.join(PREPROC_PATH, train_valid, SCENE+".npz")

    elif DATASET == "NYUCAD":
        if not FINE_TUNE:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYUCAD_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYUCAD_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["NYUCAD_RGB_PRIOR_FINE_PREPROC"]
            else:
                PREPROC_PATH = path_dict["NYUCAD_RGB_NORMALS_FINE_PRIOR_PREPROC"]
        FILE = os.path.join(PREPROC_PATH, train_valid, SCENE+".npz")

    elif DATASET == "SUNCG":
        if not FINE_TUNE:
            if INPUT_TYPE == "rgb":
                PREPROC_PATH = path_dict["SUNCG_RGB_PRIOR_PREPROC"]
            else:
                PREPROC_PATH = path_dict["SUNCG_RGB_NORMALS_PRIOR_PREPROC"]
        else:
            print("Fine-tuning from SUNCG is only possible with NYU or NYUCAD Datasets")
            exit(-1)
        FILE = os.path.join(PREPROC_PATH, train_valid, SCENE[:2], SCENE + ".npz")
    else:
        print("Dataset", DATASET, "not supported yet!")
        exit(-1)


def preproc2obj():

    import numpy as np
    from utils.obj import vox2obj, vol2obj, weights2obj

    npz = np.load(FILE)
    vox_tsdf = npz['vox_tsdf']
    vox_grid = (abs(vox_tsdf) == 1.0).astype(np.uint8)
    vox_prior = npz['vox_prior']
    gt = npz['gt']
    vox_weights = npz['vox_weights']

    if not FINE_TUNE:
        basename = DATASET+"_"+INPUT_TYPE+"_"+os.path.basename(FILE)[:-4]
    else:
        basename = DATASET+"_"+INPUT_TYPE+"_FINE_"+os.path.basename(FILE)[:-4]


    print("Surface voxels...")
    surface_file = os.path.join(obj_dir, basename) + "_surf"
    vox2obj(surface_file, vox_grid, vox_shape, 0.02, include_top=True, triangular=False, inner_faces=True,
            complete_walls=False, colors=None, gap=0.15)

    print("GT...")
    gt_file = os.path.join(obj_dir, basename) + "_gt"
    vox2obj(gt_file, gt, vox_shape_down, 0.08, include_top=True, triangular=False, inner_faces=True,
            complete_walls=False, colors=None, gap=0.15)

    print("Surface Prior volume...")
    prior_file = os.path.join(obj_dir, basename) + "_prior"
    vox2obj(prior_file, np.argmax(vox_prior, -1), vox_shape_down, 0.08, include_top=True, triangular=False,
            inner_faces=True,
            complete_walls=False, colors=None, gap=0.15)

    print("Weights volume...")
    weights_file = os.path.join(obj_dir, basename) + "_weights"
    weights2obj(weights_file, vox_weights, vox_shape_down, 0.08, gap=0.15)

    print("Done! You can open the generated volumes with Blender!\n")



# Main Function
def main():
    parse_arguments()
    preproc2obj()


if __name__ == '__main__':
  main()

