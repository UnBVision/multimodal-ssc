
loading R272_MMNet_SUPERV-NYU-depth+rgb+normals_EPOCH_79
valid miou: 42.0: 100%|███████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [01:03<00:00, 10.27it/s]
prec rec. IoU  MIou
81.1 74.8 63.7 42.0
ceil        :  35.7        floor       :  94.0        wall        :  37.2        window      :  23.5
chair       :  33.0        bed         :  65.0        sofa        :  49.4        table       :  24.5
tvs         :  27.2        furniture   :  43.6        objects     :  28.5
Latex Line:
81.1 & 74.8 & 63.7 & 35.7 & 94.0 & 37.2 & 23.5 & 33.0 & 65.0 & 49.4 & 24.5 & 27.2 & 43.6 & 28.5 & 42.0 \\



INTERVAL 3

ALPHA .4
loading R290_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.4-Int-3.0_EPOCH_79
valid miou: 42.5: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [01:06<00:00,  9.84it/s]
prec rec. IoU  MIou
79.3 75.2 62.9 42.5
ceil        :  39.5        floor       :  92.7        wall        :  38.4        window      :  29.0
chair       :  31.0        bed         :  65.0        sofa        :  49.5        table       :  21.6
tvs         :  27.8        furniture   :  42.9        objects     :  30.5
Latex Line:
79.3 & 75.2 & 62.9 & 39.5 & 92.7 & 38.4 & 29.0 & 31.0 & 65.0 & 49.5 & 21.6 & 27.8 & 42.9 & 30.5 & 42.5 \\

ALPHA .5
loading R291_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.5-Int-3.0_EPOCH_79
valid miou: 43.4: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [01:05<00:00,  9.93it/s]
prec rec. IoU  MIou
79.2 75.9 63.3 43.4
ceil        :  38.9        floor       :  92.1        wall        :  38.5        window      :  29.6
chair       :  31.4        bed         :  65.4        sofa        :  50.9        table       :  23.1
tvs         :  33.7        furniture   :  43.2        objects     :  30.2
Latex Line:
79.2 & 75.9 & 63.3 & 38.9 & 92.1 & 38.5 & 29.6 & 31.4 & 65.4 & 50.9 & 23.1 & 33.7 & 43.2 & 30.2 & 43.4 \\

ALPHA .6
lisa-153 R28_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.6-Int-3.0
loading R28_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.6-Int-3.0_EPOCH_79
valid miou: 43.0: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:46<00:00, 14.14it/s]
prec rec. IoU  MIou
79.3 75.1 62.8 43.0
ceil        :  43.3        floor       :  92.1        wall        :  37.7        window      :  31.1
chair       :  32.3        bed         :  64.1        sofa        :  49.1        table       :  21.1
tvs         :  29.0        furniture   :  44.0        objects     :  29.5
Latex Line:
79.3 & 75.1 & 62.8 & 43.3 & 92.1 & 37.7 & 31.1 & 32.3 & 64.1 & 49.1 & 21.1 & 29.0 & 44.0 & 29.5 & 43.0 \\

ALPHA .7
ADN R292_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.7-Int-3.0
loading R294_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.7-Int-3.0_EPOCH_79
valid miou: 43.2: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [01:03<00:00, 10.22it/s]
prec rec. IoU  MIou
78.8 75.4 62.7 43.2
ceil        :  44.8        floor       :  91.8        wall        :  39.0        window      :  28.6
chair       :  32.1        bed         :  65.2        sofa        :  49.9        table       :  21.5
tvs         :  28.5        furniture   :  44.9        objects     :  28.6
Latex Line:
78.8 & 75.4 & 62.7 & 44.8 & 91.8 & 39.0 & 28.6 & 32.1 & 65.2 & 49.9 & 21.5 & 28.5 & 44.9 & 28.6 & 43.2 \\

ALPHA .8
lisa-156 R427_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.8-Int-3.0
loading R427_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.8-Int-3.0_EPOCH_79
valid miou: 42.3: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:46<00:00, 14.16it/s]
prec rec. IoU  MIou
79.7 72.9 61.5 42.3
ceil        :  42.0        floor       :  91.7        wall        :  37.9        window      :  28.6
chair       :  32.9        bed         :  62.1        sofa        :  49.6        table       :  22.8
tvs         :  27.1        furniture   :  42.4        objects     :  28.5
Latex Line:
79.7 & 72.9 & 61.5 & 42.0 & 91.7 & 37.9 & 28.6 & 32.9 & 62.1 & 49.6 & 22.8 & 27.1 & 42.4 & 28.5 & 42.3 \\

loading R429_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.9-Int-3.0_EPOCH_79
valid miou: 42.7: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:47<00:00, 13.77it/s]
prec rec. IoU  MIou
79.3 74.8 62.6 42.7
ceil        :  40.9        floor       :  91.4        wall        :  38.2        window      :  27.3
chair       :  31.2        bed         :  65.1        sofa        :  51.1        table       :  23.0
tvs         :  31.2        furniture   :  41.5        objects     :  29.2
Latex Line:
79.3 & 74.8 & 62.6 & 40.9 & 91.4 & 38.2 & 27.3 & 31.2 & 65.1 & 51.1 & 23.0 & 31.2 & 41.5 & 29.2 & 42.7 \\
Current cuda device: 0 (NVIDIA GeForce RTX 2080 Ti)

loading R431_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-1.0-Int-3.0_EPOCH_79
valid miou: 42.7: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:47<00:00, 13.87it/s]
prec rec. IoU  MIou
79.2 74.3 62.2 42.7
ceil        :  42.5        floor       :  91.5        wall        :  39.0        window      :  28.3
chair       :  31.6        bed         :  64.9        sofa        :  49.9        table       :  22.3
tvs         :  28.7        furniture   :  43.4        objects     :  28.2
Latex Line:
79.2 & 74.3 & 62.2 & 42.5 & 91.5 & 39.0 & 28.3 & 31.6 & 64.9 & 49.9 & 22.3 & 28.7 & 43.4 & 28.2 & 42.7 \\


INTERVAL 5

lisa-153 R26_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.4-Int-5.0_EPOCH_79
loading R26_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.4-Int-5.0_EPOCH_79
valid miou: 43.1: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:45<00:00, 14.53it/s]
prec rec. IoU  MIou
80.1 74.7 63.0 43.1
ceil        :  39.9        floor       :  93.2        wall        :  38.2        window      :  28.8
chair       :  33.4        bed         :  64.6        sofa        :  50.8        table       :  23.1
tvs         :  29.8        furniture   :  42.3        objects     :  29.9
Latex Line:
80.1 & 74.7 & 63.0 & 39.9 & 93.2 & 38.2 & 28.8 & 33.4 & 64.6 & 50.8 & 23.1 & 29.8 & 42.3 & 29.9 & 43.1 \\

lisa-153 R27_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.5-Int-5.0_EPOCH_79
loading R27_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.5-Int-5.0_EPOCH_79
valid miou: 43.1: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:45<00:00, 14.35it/s]
prec rec. IoU  MIou
79.3 76.1 63.5 43.1
ceil        :  39.1        floor       :  93.0        wall        :  39.0        window      :  28.6
chair       :  32.7        bed         :  65.9        sofa        :  50.7        table       :  22.2
tvs         :  28.7        furniture   :  43.8        objects     :  30.7
Latex Line:
79.3 & 76.1 & 63.5 & 39.1 & 93.0 & 39.0 & 28.6 & 32.7 & 65.9 & 50.7 & 22.2 & 28.7 & 43.8 & 30.7 & 43.1 \\


loading R268_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.6-Int-5_EPOCH_79
valid miou: 43.5: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [01:04<00:00, 10.18it/s]
prec rec. IoU  MIou
78.8 76.0 63.1 43.5
ceil        :  41.7        floor       :  92.9        wall        :  38.0        window      :  28.7
chair       :  33.3        bed         :  65.4        sofa        :  50.2        table       :  24.2
tvs         :  29.1        furniture   :  44.4        objects     :  30.3
Latex Line:
78.8 & 76.0 & 63.1 & 41.7 & 92.9 & 38.0 & 28.7 & 33.3 & 65.4 & 50.2 & 24.2 & 29.1 & 44.4 & 30.3 & 43.5 \\

lisa-156 R425_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.6-Int-5.0_EPOCH_79
loading R425_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.6-Int-5.0_EPOCH_79
valid miou: 42.5: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:45<00:00, 14.34it/s]
prec rec. IoU  MIou
79.7 74.9 62.9 42.5
ceil        :  39.1        floor       :  92.9        wall        :  38.6        window      :  29.6
chair       :  31.5        bed         :  64.8        sofa        :  49.4        table       :  21.4
tvs         :  27.6        furniture   :  42.9        objects     :  30.0
Latex Line:
79.7 & 74.9 & 62.9 & 39.1 & 92.9 & 38.6 & 29.6 & 31.5 & 64.8 & 49.4 & 21.4 & 27.6 & 42.9 & 30.0 & 42.5 \\

loading R305_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.6-Int-5.0_EPOCH_59 **60 epochs
valid miou: 43.8: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [01:06<00:00,  9.83it/s]
prec rec. IoU  MIou
80.3 74.1 62.7 43.8
ceil        :  43.7        floor       :  93.1        wall        :  37.0        window      :  29.9
chair       :  34.1        bed         :  64.4        sofa        :  50.7        table       :  23.7
tvs         :  32.2        furniture   :  43.3        objects     :  30.1
Latex Line:
80.3 & 74.1 & 62.7 & 43.7 & 93.1 & 37.0 & 29.9 & 34.1 & 64.4 & 50.7 & 23.7 & 32.2 & 43.3 & 30.1 & 43.8 \\


lisa-156 R426_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.7-Int-5.0_EPOCH_79
loading R426_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.7-Int-5.0_EPOCH_79
valid miou: 42.7: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:45<00:00, 14.43it/s]
prec rec. IoU  MIou
79.5 74.1 62.2 42.7
ceil        :  42.8        floor       :  92.5        wall        :  37.7        window      :  28.2
chair       :  33.0        bed         :  63.1        sofa        :  49.5        table       :  21.6
tvs         :  26.9        furniture   :  43.6        objects     :  30.7
Latex Line:
79.5 & 74.1 & 62.2 & 42.8 & 92.5 & 37.7 & 28.2 & 33.0 & 63.1 & 49.5 & 21.6 & 26.9 & 43.6 & 30.7 & 42.7 \\

lisa-153 R30_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.8-Int-5.0
loading R30_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.8-Int-5.0_EPOCH_79
valid miou: 43.0: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:45<00:00, 14.32it/s]
prec rec. IoU  MIou
79.1 75.6 63.0 43.0
ceil        :  40.7        floor       :  92.5        wall        :  39.1        window      :  28.6
chair       :  32.5        bed         :  64.2        sofa        :  50.9        table       :  21.3
tvs         :  30.3        furniture   :  43.0        objects     :  30.1
Latex Line:
79.1 & 75.6 & 63.0 & 40.7 & 92.5 & 39.1 & 28.6 & 32.5 & 64.2 & 50.9 & 21.3 & 30.3 & 43.0 & 30.1 & 43.0 \\

loading R31_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.9-Int-5.0_EPOCH_79
valid miou: 42.6: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:45<00:00, 14.48it/s]
prec rec. IoU  MIou
79.2 75.1 62.8 42.6
ceil        :  41.5        floor       :  92.3        wall        :  38.7        window      :  28.1
chair       :  31.8        bed         :  64.1        sofa        :  50.4        table       :  23.0
tvs         :  24.7        furniture   :  44.9        objects     :  29.1
Latex Line:
79.2 & 75.1 & 62.8 & 41.5 & 92.3 & 38.7 & 28.1 & 31.8 & 64.1 & 50.4 & 23.0 & 24.7 & 44.9 & 29.1 & 42.6 \\

loading R32_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-1.0-Int-5.0_EPOCH_79
valid miou: 43.4: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:45<00:00, 14.44it/s]
prec rec. IoU  MIou
78.5 76.8 63.5 43.4
ceil        :  39.8        floor       :  92.1        wall        :  38.6        window      :  30.1
chair       :  31.5        bed         :  65.2        sofa        :  51.7        table       :  23.4
tvs         :  28.7        furniture   :  45.6        objects     :  30.3
Latex Line:
78.5 & 76.8 & 63.5 & 39.8 & 92.1 & 38.6 & 30.1 & 31.5 & 65.2 & 51.7 & 23.4 & 28.7 & 45.6 & 30.3 & 43.4 \\


INTERVAL 6

loading R301_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-1.0-Int-6.0_EPOCH_79
valid miou: 42.0: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [01:06<00:00,  9.87it/s]
prec rec. IoU  MIou
79.7 73.0 61.6 42.0
ceil        :  40.2        floor       :  92.3        wall        :  37.6        window      :  27.6
chair       :  31.9        bed         :  63.3        sofa        :  50.5        table       :  22.3
tvs         :  24.1        furniture   :  43.0        objects     :  29.0
Latex Line:
79.7 & 73.0 & 61.6 & 40.2 & 92.3 & 37.6 & 27.6 & 31.9 & 63.3 & 50.5 & 22.3 & 24.1 & 43.0 & 29.0 & 42.0 \\


INTERVAL 7

lisa-156 R428_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.4-Int-7.0
loading R428_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.4-Int-7.0_EPOCH_79
valid miou: 42.2: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [00:46<00:00, 13.96it/s]
prec rec. IoU  MIou
80.0 74.1 62.5 42.2
ceil        :  34.6        floor       :  93.6        wall        :  37.6        window      :  29.0
chair       :  32.3        bed         :  63.2        sofa        :  50.2        table       :  23.0
tvs         :  28.1        furniture   :  43.1        objects     :  29.4
Latex Line:
80.0 & 74.1 & 62.5 & 34.6 & 93.6 & 37.6 & 29.0 & 32.3 & 63.2 & 50.2 & 23.0 & 28.1 & 43.1 & 29.4 & 42.2 \\




########################
da
########################


loading R277_MMNet_SUPERV-NYU-depth+rgb+normals_da_EPOCH_79
valid miou: 44.2: 100%|███████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [01:03<00:00, 10.27it/s]
prec rec. IoU  MIou
80.9 76.2 64.6 44.2
ceil        :  33.5        floor       :  94.2        wall        :  32.9        window      :  24.5
chair       :  37.8        bed         :  68.2        sofa        :  53.0        table       :  27.2
tvs         :  33.4        furniture   :  47.6        objects     :  34.2
Latex Line:
80.9 & 76.2 & 64.6 & 33.5 & 94.2 & 32.9 & 24.5 & 37.8 & 68.2 & 53.0 & 27.2 & 33.4 & 47.6 & 34.2 & 44.2 \\

loading R278_MMNet_SEMISUP-NYU-depth+rgb+normals-Alp-0.6-Int-5.0_da_EPOCH_79
valid miou: 44.2: 100%|███████████████████████████████████████████████████████████████████████████████████████████████| 654/654 [01:03<00:00, 10.26it/s]
prec rec. IoU  MIou
81.8 74.7 64.0 44.2
ceil        :  35.3        floor       :  93.8        wall        :  32.3        window      :  26.4
chair       :  37.4        bed         :  67.7        sofa        :  51.8        table       :  25.1
tvs         :  35.4        furniture   :  47.6        objects     :  32.7
Latex Line:
81.8 & 74.7 & 64.0 & 35.3 & 93.8 & 32.3 & 26.4 & 37.4 & 67.7 & 51.8 & 25.1 & 35.4 & 47.6 & 32.7 & 44.2 \\

