import numpy as np


class OneCycleLr:
    def __init__(self, optimizer, lr_factors, milestones, writer=None):

        assert len(milestones) == len(lr_factors), "milestones and lr_factors should be of same size"

        for i in range(len(milestones) - 1):
            assert milestones[i] < milestones[i + 1], "milestone values should be a crescent list"

        self.optimizer = optimizer
        self.base_lrs = np.array([x['lr'] for x in optimizer.param_groups])
        self.lr_factors = np.array([1.] + lr_factors)
        self.milestones = np.array([0] + milestones)
        self.writer = writer

    def update(self, epoch):

        current_lrs = self.base_lrs.copy()
        lr_phase = 0
        for lr_phase in range(len(self.milestones)):
            if epoch < self.milestones[lr_phase]:
                break
            current_lrs *= self.lr_factors[lr_phase]

        epoch_delta = epoch - self.milestones[lr_phase - 1]

        factor_delta = self.lr_factors[lr_phase] - self.lr_factors[lr_phase - 1]

        milestone_delta = self.milestones[lr_phase] - self.milestones[lr_phase - 1]

        factor = self.lr_factors[lr_phase - 1] + epoch_delta * factor_delta / milestone_delta

        current_lrs = self.base_lrs * factor

        # print("###############################")

        # print("epoch", epoch)
        # print("lr_phase", lr_phase)
        # print("epoch_d", epoch_delta)
        # print("factor_d", factor_delta)
        # print("milestone_d", milestone_delta)
        # print("factor", factor)
        # print("base", self.base_lrs)
        # print("lrs", current_lrs)

        for i, (param_group, lr) in enumerate(zip(self.optimizer.param_groups, current_lrs)):
            param_group['lr'] = lr

            if self.writer is not None:
                self.writer.add_scalar('LR/{}'.format(i), lr, epoch)

        return current_lrs
